﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace CarRentals
{
    /// <summary>
    /// Interaction logic for Vehicles.xaml
    /// </summary>
    public partial class Vehicles : UserControl
    {
        public MainWindow parentWindow;
        private static bool firstLoad = true;
        private Vehicle pendingAdd;
        private List<string> headers = new List<string>() { "ID", "Type", "Branch", "Make", "Model", "VIN" };
        private List<string> bindings = new List<string>() { "id", "tid", "bid", "make", "model", "vin" };
        public Vehicles()
        {
            InitializeComponent();
        }

        #region Event Behavior
        /// <summary>
        /// Loads and sets database context for lifetime of application.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void DG_Loaded(object sender, RoutedEventArgs e)
        {
            if (firstLoad)
            {
                firstLoad = false;//jank but whatever; can't use Initialized event because we need the window to be loaded (access progress bar)
                parentWindow = (MainWindow)Window.GetWindow(this);
                parentWindow.StartProgressBar();

                using (var context = new car_rental_systemEntities())
                {
                    if (await DatabaseHandler.TestConnectionAsync(context))
                    {
                        try
                        {
                            //Testing code
                            if (parentWindow.testing)
                                await Task.Delay(700); //artificial loading time
                            var result = QueryVehicle(context);
                            DataGrid_Vehicles.ItemsSource = new ObservableCollection<Vehicle>(result);// await query.ToListAsync();//
                            GenerateTable();
                            AddRow();
                        }

                        catch (Exception err)
                        {
                            ShowException(err);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Connection to SQL Server could not be established.");
                    }
                }
                parentWindow.StopProgressBar();
                e.Handled = true;
            }
            else
            { // fix this stupid jank later
                if (parentWindow.selectedBranch != null)
                {
                    var result = QueryVehicleBranch(new car_rental_systemEntities());
                    DataGrid_Vehicles.ItemsSource = new ObservableCollection<Vehicle>(result);// await query.ToListAsync();//
                    AddRow();
                }
                else
                {
                    var result = QueryVehicle(new car_rental_systemEntities());
                    DataGrid_Vehicles.ItemsSource = new ObservableCollection<Vehicle>(result);// await query.ToListAsync();//
                    AddRow();
                }
            }
        }
        /// <summary>
        /// Catch special keypresses to pseudooverride them.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>
        /// There is a difference between previewkeydown and keydown:
        /// https://blogs.msdn.microsoft.com/jfoscoding/2006/01/26/a-new-way-of-saying-isinputkey-previewkeydown/
        /// An alternative, more cleaner way, would be to use DataGrid.InputBindings and KeyBinding="Enter" with 
        /// specified Command="{Binding [class]}". But the more I never have to look at XAML the happier I am.
        /// </remarks>
        private async void DG_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            var dg = (DataGrid)sender;
            if (e.Key.Equals(Key.Enter))
            {
                //Send new entry
                if (dg.SelectedIndex == ((ObservableCollection<Vehicle>)dg.ItemsSource).Count - 1)
                {
                    if (!pendingAdd.Null())
                    {
                        dg.SelectedItem = null; //deselect

                        using (var context = new car_rental_systemEntities())
                        {
                            try
                            {
                                await QueryAdd(context);
                                AddRow();
                            }
                            catch (Exception err)
                            {
                                ShowException(err);
                            }
                        }
                    }
                    else
                        MessageBox.Show("Invalid vehicle entry");
                }
                //Update current entry
                else
                {
                    var row = (Vehicle)dg.SelectedItems[0];
                    if (!row.Null())
                    {
                        using (var context = new car_rental_systemEntities())
                        {
                            try
                            {
                                await QueryUpdate(context, row);
                            }
                            catch (Exception err)
                            {
                                ShowException(err);
                            }
                        }
                    }
                    else
                        MessageBox.Show("ID, Make, Model, and VIN cannot be empty.");
                }
            }
            if (e.Key.Equals(Key.Delete))
            {
                //if nothing selected and not the new entry row
                var index = dg.SelectedIndex;
                if (index != -1 && dg.SelectedIndex != pendingAdd.id - 1)
                {
                    using (var context = new car_rental_systemEntities())
                    {
                        try
                        {
                            await QueryDelete(context, ((Vehicle)dg.SelectedItems[0]).id);
                            DeleteRow(index);
                        }
                        catch (Exception err)
                        {
                            ShowException(err);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// With UpdateSourceTrigger = PropertyChanged, this updates bound cells the moment any change is detected.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DG_OnCellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            var dg = (DataGrid)sender;
            //Behavior on new entries (last row)
            if (dg.SelectedIndex != -1 && dg.SelectedIndex == (pendingAdd.id - 1))
            {
                var row = (Vehicle)dg.SelectedItems[0];
                pendingAdd.bid = row.bid;
                pendingAdd.tid = row.tid;
                pendingAdd.make = row.make;
                pendingAdd.model = row.model;
                pendingAdd.vin = row.vin;
            }
            //Behavior on existing entries
        }
        #endregion

        #region Queries
        /// <summary>
        /// Query but return non-anonymous list.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private List<Vehicle> QueryVehicle(car_rental_systemEntities context)
        {
            var vehicles = context.Vehicles;
            var query =
                from v in vehicles
                select new { v.id, v.tid, v.bid, v.make, v.model, v.vin }; //only get out what we want from query

            //Throw out the other data because WPF doesn't like anonymous queries
            //See: https://stackoverflow.com/questions/1094931/linq-to-sql-how-to-select-specific-columns-and-return-strongly-typed-list
            List<Vehicle> items = query.AsEnumerable().Select(o => new Vehicle
            {
                id = o.id,
                tid = o.tid,
                bid = o.bid,
                make = o.make,
                model = o.model,
                vin = o.vin
            }).ToList();

            return items;
        }

        /// <summary>
        /// Query but return non-anonymous list.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private List<Vehicle> QueryVehicleBranch(car_rental_systemEntities context)
        {
            var branch = DatabaseHandler.branchToID[parentWindow.selectedBranch];
            var vehicles = context.Vehicles;
            var query =
                from v in vehicles
                where v.bid == branch
                select new { v.id, v.tid, v.bid, v.make, v.model, v.vin }; //only get out what we want from query

            //Throw out the other data because WPF doesn't like anonymous queries
            //See: https://stackoverflow.com/questions/1094931/linq-to-sql-how-to-select-specific-columns-and-return-strongly-typed-list
            List<Vehicle> items = query.AsEnumerable().Select(o => new Vehicle
            {
                id = o.id,
                tid = o.tid,
                bid = o.bid,
                make = o.make,
                model = o.model,
                vin = o.vin
            }).ToList();

            return items;
        }

        /// <summary>
        /// Deletes an entry from the database via an attatched stub.
        /// </summary>
        /// <param name="context"></param>
        /// <param name=""></param>
        private async Task<int> QueryDelete(car_rental_systemEntities context, int primaryKey)
        {
            var delete = new Vehicle { id = primaryKey }; //the stub
            context.Vehicles.Attach(delete);
            context.Vehicles.Remove(delete);
            return await context.SaveChangesAsync();
        }

        /// <summary>
        /// Adds to database.
        /// </summary>
        /// <param name="context"></param>
        /// <remarks>
        /// Currently has a bug where it's not getting pushed with the right id
        /// </remarks>
        private async Task<int> QueryAdd(car_rental_systemEntities context)
        {
            //var branch = await context.Vehicles.FirstOrDefaultAsync(o => o.bid == pendingAdd.bid);
            //var type = await context.Vehicles.FirstOrDefaultAsync(o => o.tid == pendingAdd.tid);
            context.Vehicles.Add(pendingAdd);
            return await context.SaveChangesAsync();
        }

        /// <summary>
        /// Makes changes to a given row and pushes results to the database.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="update"></param>
        /// <returns></returns>
        private async Task<int> QueryUpdate(car_rental_systemEntities context, Vehicle update)
        {
            var result = await context.Vehicles.SingleAsync(o => o.id == update.id);
            if (result != null)
            {
                result.id = update.id;
                result.bid = update.bid;
                result.tid = update.tid;
                result.make = update.make;
                result.model = update.model;
                result.vin = update.vin;

                return await context.SaveChangesAsync();
            }
            else throw new NullReferenceException();
        }
        #endregion

        /// <summary>
        /// Generates columns and headers for vehicles table.
        /// Because I'm tired of dealing with XAML.
        /// </summary>
        private void GenerateTable()
        {
            /*for (int i = 0; i < headers.Count; i++)
            {
                var col = new DataGridTextColumn();
                if (i == 0)
                    col.MaxWidth = 35;
                col.Header = headers[i];
                var bind = new Binding(bindings[i]);
                bind.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                col.Binding = bind;
                DataGrid_Vehicles.Columns.Add(col);
            }*/
            var context = new car_rental_systemEntities();
            BranchColumn.ItemsSource = context.Branches.ToList();
            TypeColumn.ItemsSource = context.VehicleTypes.ToList();
        }

        /// <summary>
        /// Adds row with ID prefilled with max row.
        /// </summary>
        private void AddRow()
        {
            var vehicles = (ObservableCollection<Vehicle>)DataGrid_Vehicles.ItemsSource;
            var maxValue = (from r in new car_rental_systemEntities().Rentals
                            select r.id).Max();
            pendingAdd = new Vehicle { id = maxValue };
            vehicles.Add(pendingAdd);
            //Below two commit edits are necessary. Weird bug with DataGrid class, see below:
            //https://stackoverflow.com/questions/20204592/wpf-datagrid-refresh-is-not-allowed-during-an-addnew-or-edititem-transaction-m
            DataGrid_Vehicles.CommitEdit();
            DataGrid_Vehicles.CommitEdit();
            DataGrid_Vehicles.Items.Refresh();
        }

        /// <summary>
        /// Removes row at index.
        /// </summary>
        /// <param name="index"></param>
        private void DeleteRow(int index)
        {
            var vehicles = (ObservableCollection<Vehicle>)DataGrid_Vehicles.ItemsSource;
            vehicles.RemoveAt(index);
            DataGrid_Vehicles.CommitEdit();
            DataGrid_Vehicles.CommitEdit();
            DataGrid_Vehicles.Items.Refresh();
        }

        /// <summary>
        /// Removes item in DataGrid
        /// </summary>
        /// <param name="item"></param>
        private void DeleteRow(Vehicle item)
        {
            var vehicles = (ObservableCollection<Vehicle>)DataGrid_Vehicles.ItemsSource;
            vehicles.Remove(item);
            DataGrid_Vehicles.CommitEdit();
            DataGrid_Vehicles.CommitEdit();
            DataGrid_Vehicles.Items.Refresh();
        }

        /// <summary>
        /// Handles pass exceptions to show as message box and output to console.
        /// </summary>
        /// <param name="e"></param>
        private void ShowException(Exception e)
        {
            MessageBox.Show("This error will appear in output console as well:\n\n" + e.ToString());
            Console.WriteLine(e);
        }
    }
}
