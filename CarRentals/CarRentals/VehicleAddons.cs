﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentals
{
    public partial class Vehicle
    {
        public bool Null()
        {
            return id <= 0 || tid <= 0 || bid <= 0 || make == null || model == null || vin == null ||
                make.Equals("") || model.Equals("") || vin.Equals("");
        }
    }
}
