﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CarRentals
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool loading = true;
        public bool testing = true;
        public string selectedBranch;
        public bool changedTab = false;

        public MainWindow()
        {
            InitializeComponent();
        }

        public void SetBranch()
        {
            status_label.Content = selectedBranch;
        }

        public async void MW_Loaded(object sender, RoutedEventArgs e)
        {
            using (var context = new car_rental_systemEntities())
            {
                if (await DatabaseHandler.TestConnectionAsync(context))
                {
                    try
                    {
                        DatabaseHandler.QueryDictionaries(context);
                    }

                    catch (Exception err)
                    {
                        ShowException(err);
                    }
                }
                else
                {
                    MessageBox.Show("Connection to SQL Server could not be established.");
                }
            }
        }

        /// <summary>
        /// Function that stops the progress bar.
        /// </summary>
        /// <remarks>
        /// Is below a bad way of doing this? Yes.
        /// Does using event handlers, routedevents, and whatever work? No. 
        /// 
        /// For some reason I can't access this user control from MainWindow, 
        /// making it literally impossible to subscribe any events defined from here.
        /// I'm 99% sure it has something to do with the way the xaml is set up.
        /// </remarks>
        public void StopProgressBar()
        {
            loading = false;
        }

        /// <summary>
        /// Function that starts the progress bar.
        /// </summary>
        public void StartProgressBar()
        {
            MainWindow_StartProgressBar(this, EventArgs.Empty);
        }

        /// <summary>
        /// Starts update loop for the progress bar.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_StartProgressBar(object sender, EventArgs e)
        {
            loading = true;
            ProgressBar.IsIndeterminate = true;
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += worker_DoWork;
            worker.ProgressChanged += worker_ProgressChanged;
            worker.RunWorkerCompleted += worker_WorkCompleted;
            worker.RunWorkerAsync();
        }

        /// <summary>
        /// Work in background thread rather than UI thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>
        /// https://docs.microsoft.com/en-us/dotnet/api/system.componentmodel.backgroundworker?view=netframework-4.8
        /// </remarks>
        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            
            for (var i = 0; loading == true; i++)
            {
                (sender as BackgroundWorker).ReportProgress(i);
                Thread.Sleep(100);
            }
        }
        
        /// <summary>
        /// Update on progress bar changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            ProgressBar.Value = e.ProgressPercentage;
        }

        /// <summary>
        /// When work is completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_WorkCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ProgressBar.IsIndeterminate = false;
            ProgressBar.Value = 0;
        }

        /// <summary>
        /// Runs initially once content is fully rendered and then every time tab is changed.
        /// It's kind of fake but not really.
        /// 
        /// Should stop using it and move all start progress bars to actual Loaded functions of each tabs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TabControl_ChangeTab(object sender, EventArgs e)
        {
            //MainWindow_StartProgressBar(sender, e);

            if (VehiclesTab.IsSelected)
            {
                changedTab = true;
                Console.WriteLine("hi");
            }
            else
            {
                changedTab = false;
                Console.WriteLine("off");
            }
            //else if (RentalsTab.IsSelected)
            //{

            //}
            //else if (ReportsTab.IsSelected)
            //{

            //}
        }

        /// <summary>
        /// Handles pass exceptions to show as message box and output to console.
        /// </summary>
        /// <param name="e"></param>
        private void ShowException(Exception e)
        {
            MessageBox.Show("This error will appear in output console as well:\n\n" + e.ToString());
            Console.WriteLine(e);
        }
    }
}
