﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentals
{
    public class RentalRelation : INotifyCollectionChanged, INotifyPropertyChanged
    {
        public int id { get; set; }
        public int eid { get; set; }
        public int bid { get; set; }
        public int cid { get; set; }
        public int vid { get; set; }
        public System.DateTime dueDate { get; set; } //{ get { return dueDate.Date; } set { dueDate = value; } }
        public System.DateTime pickupDate { get; set; }//{ get { return pickupDate.Date; } set { pickupDate = value; } }
        public string isGold { get; set; }
        public string eName { get; set; }
        public string bName { get; set; }
        public string cName { get; set; }
        public string vin { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public RentalRelation()
        {
        }
        
        public RentalRelation(Rental o)
        {
            id = o.id;
            eid = o.eid;
            bid = o.bid;
            cid = o.cid;
            vid = o.vid;
            dueDate = o.dueDate;
            pickupDate = o.pickupDate;
            //isGold will be null
            eName = DatabaseHandler.idToEmployee[o.eid];
            bName = DatabaseHandler.idToBranch[o.bid];
            cName = DatabaseHandler.idToCustomer[o.cid];
            vin = DatabaseHandler.idToVIN[o.vid];
        }

        protected void OnPropertyChanged(string o)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(o));
        }

        protected void OnCollectionChanged(NotifyCollectionChangedAction o)
        {
            if (CollectionChanged != null)
                CollectionChanged(this, new NotifyCollectionChangedEventArgs(o));
        }

        public static explicit operator RentalRelation(Rental o)
        {
            RentalRelation rentalRelation = new RentalRelation(o);
            return rentalRelation;
        }

        public static explicit operator Rental(RentalRelation o)
        {
            Rental rental = new Rental(o);
            return rental;
        }
    }
}
