﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows;

namespace CarRentals
{
    public sealed class DatabaseHandler
    {
        private static readonly DatabaseHandler instance = new DatabaseHandler();

        /// <summary>
        /// Explicit static constructor to tell C# compiler not to mark type as beforefieldinit  
        /// </summary>
        /// <remarks>
        /// Below static constructor is intentional
        /// See fourth implementation:
        /// https://csharpindepth.com/Articles/Singleton
        /// </remarks>
        static DatabaseHandler() { }
        private DatabaseHandler() { }

        public static DatabaseHandler Instance { get { return instance; } }

        //store candidate keys here things here, since they are unique anyway
        //rather than query the tables every time
        public static Dictionary<int, string> idToBranch = new Dictionary<int, string>();
        public static Dictionary<string, int> branchToID = new Dictionary<string, int>();
        public static Dictionary<int, string> idToType = new Dictionary<int, string>();
        public static Dictionary<string, int> typeToID = new Dictionary<string, int>();
        public static Dictionary<int, string> idToVIN = new Dictionary<int, string>();
        public static Dictionary<string, int> vinToID = new Dictionary<string, int>();
        //one-directional dictionaries
        public static Dictionary<int, string> idToEmployee = new Dictionary<int, string>();
        public static Dictionary<int, string> idToCustomer = new Dictionary<int, string>();

        /// <summary>
        /// Queries database to populate ID to Name conversion dictionaries.
        /// </summary>
        /// <param name="context"></param>
        public static void QueryDictionaries(car_rental_systemEntities context)
        {
            //jank but in a rush. Could do a ton of left joins, but it will look grosser than this
            var vehicles = (from v in context.Vehicles
                           select new { v.id, v.vin }).Distinct();
            var customers = (from c in context.Customers
                            select new { c.id, name = c.fName + " " + c.mName + " " + c.lName }).Distinct();
            var employees = (from e in context.Employees
                            select new { e.id, name = e.fName + " " + e.mName + " " + e.lName }).Distinct();
            var branches = (from b in context.Branches
                            select new { b.id, b.location }).Distinct();
            var types = (from t in context.VehicleTypes
                            select new { t.id, t.carType }).Distinct();

            foreach (var o in branches.AsEnumerable().ToList())
            {
                if (!idToBranch.ContainsKey(o.id)) idToBranch.Add(o.id, o.location);
                if (!branchToID.ContainsKey(o.location)) branchToID.Add(o.location, o.id);
            }
            foreach (var o in types.AsEnumerable().ToList())
            {
                if (!idToType.ContainsKey(o.id)) idToType.Add(o.id, o.carType);
                if (!typeToID.ContainsKey(o.carType)) typeToID.Add(o.carType, o.id);
            }
            foreach (var o in vehicles.AsEnumerable().ToList())
            {
                if (!idToVIN.ContainsKey(o.id)) idToVIN.Add(o.id, o.vin);
                if (!vinToID.ContainsKey(o.vin)) vinToID.Add(o.vin, o.id);
            }
            foreach (var o in customers.AsEnumerable().ToList())
                if (!idToCustomer.ContainsKey(o.id)) idToCustomer.Add(o.id, o.name);
            foreach (var o in employees.AsEnumerable().ToList())
                if (!idToEmployee.ContainsKey(o.id)) idToEmployee.Add(o.id, o.name);
            //var query = from v in context.Vehicles
            //            join b in context.Branches on v.bid equals b.id
            //            join t in context.VehicleTypes on v.tid equals t.id //"on... equals ..." is equivalent to "where ... = ..."
            //            let bid = b.id
            //            let tid = t.id
            //            let vid = v.id //"let" is equivalent to "as"
            //            select new { bid, b.location, tid, t.carType, vid, v.vin };

            //foreach (var o in query.AsEnumerable().ToList())
            //{
            //    if (!idToBranch.ContainsKey(o.bid)) idToBranch.Add(o.bid, o.location);
            //    if (!branchToID.ContainsKey(o.location)) branchToID.Add(o.location, o.bid);
            //    if (!idToType.ContainsKey(o.tid)) idToType.Add(o.tid, o.carType);
            //    if (!typeToID.ContainsKey(o.carType)) typeToID.Add(o.carType, o.tid);
            //    if (!idToVIN.ContainsKey(o.vid)) idToVIN.Add(o.vid, o.vin);
            //}
        }

        /// <summary>
        /// Attempts a test connection to the database
        /// </summary>
        /// <returns>Returns false if failure</returns>
        public static async Task<bool> TestConnectionAsync(car_rental_systemEntities context)
        {
            //Better to create new db context every request
            //https://stackoverflow.com/questions/13551100/when-should-i-create-a-new-dbcontext
            if (context == null)
                throw new NullReferenceException("Context was not given in when testing connection.");

            try
            {
                await context.Database.Connection.OpenAsync();
                context.Database.Connection.Close();
            }
            catch (SqlException)
            {
                return false;
            }
            return true;
        }

    }

}
