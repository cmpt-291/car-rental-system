﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentals
{
    public partial class Rental
    {
        public Rental()
        {

        }

        public Rental(RentalRelation source)
        {
            id = source.id;
            eid = source.eid;
            bid = source.bid;
            cid = source.cid;
            vid = source.vid;
            dueDate = source.dueDate;
            pickupDate = source.pickupDate;
        }

        public static explicit operator RentalRelation(Rental o)
        {
            RentalRelation rentalRelation = new RentalRelation(o);
            return rentalRelation;
        }

        public static explicit operator Rental(RentalRelation o)
        {
            System.Console.WriteLine("Conversion occurred.");
            Rental rental = new Rental(o);
            return rental;
        }

        public bool Null()
        {
            return id <= 0 || eid <= 0 || bid <= 0 || cid <= 0 || vid <= 0 ||
                dueDate.Equals(new DateTime()) || pickupDate.Equals(new DateTime()); //|| rental.dueDate < DateTime.Now;
        }

        public override string ToString()
        {
            return String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}", id, eid, bid, cid, vid, dueDate, pickupDate);
        }
    }
}
