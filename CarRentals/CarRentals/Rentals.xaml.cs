﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using Microsoft.Maps.MapControl.WPF;

namespace CarRentals
{
    /// <summary>
    /// Interaction logic for Rentals.xaml
    /// </summary>
    public partial class Rentals : UserControl
    {
        private MainWindow parentWindow;
        private bool firstLoad = true;
        private Rental pendingAdd;
        public Rentals()
        {
            InitializeComponent();
        }

        private void Pushpin_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            parentWindow.selectedBranch = ((Pushpin)sender).Content.ToString();
            parentWindow.SetBranch();
            Rentals_Expander.IsExpanded = true;
            Rentals_DataGrid.ItemsSource = QueryRentals(((Pushpin)sender).Content.ToString());
            AddRow();
        }

        private void Map_MouseButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            parentWindow.selectedBranch = null;
            parentWindow.SetBranch();
            Rentals_Expander.IsExpanded = false;
        }

        
        /// <summary>
        /// Catch special keypresses to pseudooverride them.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>
        /// There is a difference between previewkeydown and keydown:
        /// https://blogs.msdn.microsoft.com/jfoscoding/2006/01/26/a-new-way-of-saying-isinputkey-previewkeydown/
        /// An alternative, more cleaner way, would be to use DataGrid.InputBindings and KeyBinding="Enter" with 
        /// specified Command="{Binding [class]}". But the more I never have to look at XAML the happier I am.
        /// </remarks>
        private async void DG_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            var dg = (DataGrid)sender;
            if (e.Key.Equals(Key.Enter))
            {
                Console.WriteLine(dg.SelectedIndex);
                //Send new entry
                if (dg.SelectedIndex == ((List<RentalRelation>)dg.ItemsSource).Count - 1)
                {
                    Console.WriteLine(pendingAdd.Null());
                    if (pendingAdd.Null())
                    {
                        dg.SelectedItem = null; //deselect

                        using (var context = new car_rental_systemEntities())
                        {
                            try
                            {
                                await QueryAdd(context);
                                AddRow();
                            }
                            catch (Exception err)
                            {
                                ShowException(err);
                            }
                        }
                    }
                    else
                        MessageBox.Show("Invalid rental entry");
                }
                //Update current entry
                else
                {
                    var row = new Rental((RentalRelation)dg.SelectedItems[0]); //I know what you're thinking. And I don't care.
                    Console.WriteLine(row.ToString());
                    if (!row.Null())
                    {
                        using (var context = new car_rental_systemEntities())
                        {
                            try
                            {
                                await QueryUpdate(context, row);
                            }
                            catch (Exception err)
                            {
                                ShowException(err);
                            }
                        }
                    }
                    else
                        MessageBox.Show("Invalid update entry.");
                }
            }
            if (e.Key.Equals(Key.Delete))
            {
                //if nothing selected and not the new entry row
                var index = dg.SelectedIndex;
                if (index != -1 && dg.SelectedIndex != pendingAdd.id - 1)
                {
                    using (var context = new car_rental_systemEntities())
                    {
                        try
                        {
                            await QueryDelete(context, ((RentalRelation)dg.SelectedItem).id);
                            DeleteRow(index);
                        }
                        catch (Exception err)
                        {
                            ShowException(err);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// With UpdateSourceTrigger = PropertyChanged, this updates bound cells the moment any change is detected.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DG_OnCellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            var dg = (DataGrid)sender;
            //Behavior on new entries (last row)
            if (dg.SelectedIndex != -1 && dg.SelectedIndex == ((List<RentalRelation>)dg.ItemsSource).Count - 1)
            {
                //pendingAdd = (Rental)dg.SelectedItems[0]; stupid explicit conversion implementation doesn't work for no reason
                //Console.WriteLine(pendingAdd.vid);

                var row = (RentalRelation)dg.SelectedItems[0];

                pendingAdd.eid = row.eid;
                pendingAdd.bid = row.bid;
                pendingAdd.cid = row.cid;
                pendingAdd.vid = row.vid;
                pendingAdd.dueDate = row.dueDate;
                pendingAdd.pickupDate = row.pickupDate;
                //Console.WriteLine(String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}", pendingAdd.id, pendingAdd.eid, pendingAdd.bid, pendingAdd.cid, pendingAdd.vid, pendingAdd.dueDate, pendingAdd.pickupDate));
            }
        }

        private async void Rentals_Loaded(object sender, RoutedEventArgs e)
        {
            if (firstLoad)
            {
                parentWindow = (MainWindow)Window.GetWindow(this);
                firstLoad = false;//jank but whatever; can't use Initialized event because we need the window to be loaded (access progress bar)
                parentWindow.StartProgressBar();

                using (var context = new car_rental_systemEntities())
                {
                    if (await DatabaseHandler.TestConnectionAsync(context))
                    {
                        try
                        {
                            //Testing code
                            if (parentWindow.testing)
                                await Task.Delay(700); //artificial loading time
                            var result = QueryBranch(context);
                            GeneratePushpins(result);
                        }

                        catch (Exception err)
                        {
                            ShowException(err);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Connection to SQL Server could not be established.");
                    }
                }
                parentWindow.StopProgressBar();
                e.Handled = true;
            }
        }

        #region Queries
        private List<Branch> QueryBranch(car_rental_systemEntities context)
        {
            var branches = context.Branches;
            var query = from b in branches
                      select new { b.id, b.location, b.latitude, b.longitude };

            List<Branch> items = query.AsEnumerable().Select(o => new Branch
            {
                id = o.id,
                location = o.location,
                latitude = o.latitude,
                longitude = o.longitude
            }).ToList();

            return items;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <remarks>
        /// Left outer joins:
        /// https://docs.microsoft.com/en-us/dotnet/csharp/linq/perform-left-outer-joins
        /// </remarks>
        //remember to run this after add, delete, update if it doesnt already update itself
        private List<RentalRelation> QueryRentals(string sender)
        {
            using (var context = new car_rental_systemEntities())
            {
                var branchID = DatabaseHandler.branchToID[sender];
                var query = from r in context.Rentals
                            join em in context.Employees on r.eid equals em.id
                            join c in context.Customers on r.cid equals c.id
                            join gc in context.GoldCustomers on r.cid equals gc.id into gold //left outer join apparently
                            from membership in gold.DefaultIfEmpty()
                            where r.bid == branchID
                            select new
                            {
                                r.id,
                                r.eid,
                                r.bid,
                                r.cid,
                                r.vid,
                                r.dueDate,
                                r.pickupDate,
                                goldMember = membership == null ? "False" : "True",
                            };
                List<RentalRelation> items = query.AsEnumerable().Select(o => new RentalRelation
                {
                    id = o.id,
                    eid = o.eid,
                    bid = o.bid,
                    cid = o.cid,
                    vid = o.vid,
                    dueDate = o.dueDate,
                    pickupDate = o.pickupDate,
                    eName = DatabaseHandler.idToEmployee[o.eid],
                    bName = DatabaseHandler.idToBranch[o.bid],
                    cName = DatabaseHandler.idToCustomer[o.cid],
                    vin = DatabaseHandler.idToVIN[o.vid]
                }).ToList();

                return items;
            }
        }


        /// <summary>
        /// Deletes an entry from the database via an attatched stub.
        /// </summary>
        /// <param name="context"></param>
        /// <param name=""></param>
        private async Task<int> QueryDelete(car_rental_systemEntities context, int primaryKey)
        {
            var delete = new Rental { id = primaryKey }; //the stub
            context.Rentals.Attach(delete);
            context.Rentals.Remove(delete);
            return await context.SaveChangesAsync();
        }

        /// <summary>
        /// Adds to database.
        /// </summary>
        /// <param name="context"></param>
        /// <remarks>
        /// Currently has a bug where it's not getting pushed with the right id
        /// </remarks>
        private async Task<int> QueryAdd(car_rental_systemEntities context)
        {
            context.Rentals.Add(pendingAdd);
            return await context.SaveChangesAsync();
        }

        /// <summary>
        /// Makes changes to a given row and pushes results to the database.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="update"></param>
        /// <returns></returns>
        private async Task<int> QueryUpdate(car_rental_systemEntities context, Rental update)
        {
            var result = await context.Rentals.SingleAsync(o => o.id == update.id);
            if (result != null)
            {
                result.id = update.id;
                result.eid = update.eid;
                result.bid = update.bid;
                result.cid = update.cid;
                result.vid = update.vid;
                result.dueDate = update.dueDate;
                result.pickupDate = update.pickupDate;
                return await context.SaveChangesAsync();
            }
            else throw new NullReferenceException();
        }
        #endregion

        /// <summary>
        /// Adds row with ID prefilled with max row.
        /// </summary>
        private void AddRow()
        {
            var rentals = (List<RentalRelation>)Rentals_DataGrid.ItemsSource;
            //jank; don't need the actual query, but in rush
            //though it does make running concurrent programs better at cost of taxing the server
            //versus storing a max value or whatever
            var maxValue = (from r in new car_rental_systemEntities().Rentals
                         select r.id).Max();
            var newID = maxValue + 1;
            pendingAdd = new Rental { id = newID };
            rentals.Add(new RentalRelation { id = pendingAdd.id });
            //Below two commit edits are necessary. Weird bug with DataGrid class, see below:
            //https://stackoverflow.com/questions/20204592/wpf-datagrid-refresh-is-not-allowed-during-an-addnew-or-edititem-transaction-m
            Rentals_DataGrid.CommitEdit();
            Rentals_DataGrid.CommitEdit();
            Rentals_DataGrid.Items.Refresh();
        }

        /// <summary>
        /// Removes row at index.
        /// </summary>
        /// <param name="index"></param>
        private void DeleteRow(int index)
        {
            var rentals = (List<RentalRelation>)Rentals_DataGrid.ItemsSource;
            rentals.RemoveAt(index);
            Rentals_DataGrid.CommitEdit();
            Rentals_DataGrid.CommitEdit();
            Rentals_DataGrid.Items.Refresh();
        }

        private void GeneratePushpins(List<Branch> items)
        {
            var mapLayer = new MapLayer();
            mapLayer.Name="Rentals_MapLayer";
            Rentals_Map.Children.Add(mapLayer);
            foreach (var i in items)
            {
                var pushpin = CreatePushpin(i);
                mapLayer.AddChild(pushpin, pushpin.Location);
            }
        }

        private Pushpin CreatePushpin(Branch item)
        {
            var pushPin = new Pushpin();
            pushPin.Location = new Location { Latitude = item.latitude, Longitude = item.longitude };
            pushPin.Content = item.location;
            pushPin.MouseLeftButtonDown += Pushpin_MouseLeftButtonDown;
            return pushPin;
        }
 
        /// <summary>
        /// Handles pass exceptions to show as message box and output to console.
        /// </summary>
        /// <param name="e"></param>
        private void ShowException(Exception e)
        {
            MessageBox.Show("This error will appear in output console as well:\n\n" + e.ToString());
            Console.WriteLine(e);
        }
    }
}
