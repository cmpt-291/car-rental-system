﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CarRentals
{
    /// <summary>
    /// Interaction logic for BranchRevenue.xaml
    /// </summary>
    public partial class BranchRevenue : UserControl
    {
        private MainWindow parentWindow;
        public BranchRevenue()
        {
            InitializeComponent();
        }

        private void CreateReport(DateTime ? fromDate, DateTime ? toDate)
        {
            var context = new car_rental_systemEntities();
            parentWindow = Window.GetWindow(this) as MainWindow;

            if (parentWindow.selectedBranch == null)
            {
                var results = new List<BranchRevenue_Result>();
                //do one for loop if no from year
                for (int i = 0; i < DatabaseHandler.idToBranch.Count; i++)
                {
                    var r = context.BranchRevenue(fromDate, toDate, i);
                    foreach (var item in r)
                        results.Add(item);
                }
                BR_DataGrid.ItemsSource = results;
            }
            else
            {
                //do one for loop if no from year
                var branch = DatabaseHandler.branchToID[parentWindow.selectedBranch];
                Console.WriteLine(branch);

                //BR_DataGrid.ItemsSource = context.BranchRevenue(tDate, fDate, branch).ToList();
                List<BranchRevenue_Result> results = new List<BranchRevenue_Result>();
                //do one for loop if no from year

                var r = context.BranchRevenue(fromDate, toDate, branch);
                foreach (var item in r)
                    results.Add(item);

                BR_DataGrid.ItemsSource = results;
            }
        }

        private void Generate_Click(object sender, RoutedEventArgs e)
        {
            CreateReport(DateFrom.SelectedDate, DateTo.SelectedDate);
        }
    }
}
