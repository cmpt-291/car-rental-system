﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CarRentals
{
    /// <summary>
    /// Interaction logic for BranchRevenue.xaml
    /// </summary>
    public partial class EmployeeRevenue : UserControl
    {
        private MainWindow parentWindow;
        public EmployeeRevenue()
        {
            InitializeComponent();
        }

        private void CreateReport(DateTime ? fromDate, DateTime ? toDate)
        {

            //Console.WriteLine(fDate.ToString().Substring(0, 10) + ", " + tDate.Date);

            var context = new car_rental_systemEntities();
            parentWindow = Window.GetWindow(this) as MainWindow;
            var results = new List<EmployeeRevenue_Result>();

            if (parentWindow.selectedBranch == null)
            {
                for (int i = 0; i < DatabaseHandler.idToEmployee.Count; i++)
                {
                    var r = context.EmployeeRevenue(fromDate, toDate, i);
                    foreach (var item in r)
                        results.Add(item);
                }

            }
            else
            {
                var branch = DatabaseHandler.branchToID[parentWindow.selectedBranch];
                var query = from e in new car_rental_systemEntities().Employees
                            where e.bid == branch
                            select e.id;

                foreach (var employee in query.ToList())
                {
                    var r = context.EmployeeRevenue(fromDate, toDate, employee);
                    foreach (var item in r)
                        results.Add(item);
                }
            }
            BR_DataGrid.ItemsSource = results;
        }

        private void Generate_Click(object sender, RoutedEventArgs e)
        {
            CreateReport(DateFrom.SelectedDate, DateTo.SelectedDate);
        }
    }
}
