﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CarRentals
{
    /// <summary>
    /// Interaction logic for BranchRevenue.xaml
    /// </summary>
    public partial class RentalRevenue : UserControl
    {
        private MainWindow parentWindow;
        public RentalRevenue()
        {
            InitializeComponent();
        }

        private void CreateReport(DateTime ? fromDate, DateTime ? toDate)
        {
            //Console.WriteLine(fDate.ToString().Substring(0, 10) + ", " + tDate.Date);

            var context = new car_rental_systemEntities();
            parentWindow = Window.GetWindow(this) as MainWindow;
            var results = new List<RentalRevenue_Result>();

            //do one for loop if no from year
            for (int i = 1; i <= DatabaseHandler.idToType.Count; i++)
            {
                var r = context.RentalRevenue (fromDate, toDate, DatabaseHandler.idToType[i]);
                foreach (var item in r)
                    results.Add(item);
            }
            BR_DataGrid.ItemsSource = results;
        }

        private void Generate_Click(object sender, RoutedEventArgs e)
        {
            CreateReport(DateFrom.SelectedDate, DateTo.SelectedDate);
        }
    }
}
