﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CarRentals
{
    /// <summary>
    /// Interaction logic for VehicleTypeEditor.xaml
    /// </summary>
    public partial class VehicleTypeEditor : UserControl
    {
        readonly car_rental_systemEntities entites = new car_rental_systemEntities();
        public VehicleTypeEditor()
        {
            InitializeComponent();
        }

        private void DataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            //var query = from vehicleType in entites.VehicleTypes select vehicleType;
            DataGrid.ItemsSource = entites.VehicleTypes.ToList();
        }

        private void DataGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                var n = DataGrid.SelectedItems.Count;
                var result = System.Windows.MessageBox.Show(
                    $"Are you sure you wish to delete {n} entries?",
                    "Confirm Deletion",
                    System.Windows.MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                    foreach (var item in DataGrid.SelectedItems)
                        entites.VehicleTypes.Remove(item as VehicleType);
                else e.Handled = true;
                entites.SaveChanges();
            }
        }
    }
}
