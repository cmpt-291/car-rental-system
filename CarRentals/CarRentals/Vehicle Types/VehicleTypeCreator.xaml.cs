﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CarRentals
{
    /// <summary>
    /// Interaction logic for VehicleTypeCreator.xaml
    /// </summary>
    public partial class VehicleTypeCreator : UserControl
    {
        readonly car_rental_systemEntities entites = new car_rental_systemEntities();
        public VehicleTypeCreator()
        {
            InitializeComponent();
        }

        private void Create_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var type = new VehicleType
                {
                    cost = Convert.ToDecimal(DailyFee.Text),
                    lateFee = Convert.ToDecimal(LateFee.Text),
                    branchFee = Convert.ToDecimal(BranchFee.Text),
                    carType = Description.Text
                };
                ResetFields();
                entites.VehicleTypes.Add(type);
                entites.SaveChanges();
            }
            catch (Exception ex)
            {
                Validation.Content = "One or more fields are formatted incorrectly.";
            };
        }

        private void ResetFields()
        {
            DailyFee.Text = "";
            LateFee.Text = "";
            BranchFee.Text = "";
            Description.Text = "";
            Validation.Content = "";
        }
    }
}
