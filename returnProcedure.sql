create procedure returnCost @rid int, @bid int, @eid int, @dateReturned date
as
insert into rentalReturn values(@rid, @bid, @eid, @dateReturned, 
(
select		Case
			When Rental.bid != @bid and Customer.id not in (select GoldCustomer.id from GoldCustomer)
				Then	
					(datediff(day, pickupDate, dueDate) * cost) + 
					(datediff(day, dueDate, @dateReturned) * lateFee ) +
					(branchFee)
				Else
					(datediff(day, pickupDate, dueDate) * cost) + 
					(datediff(day, dueDate, @dateReturned) * lateFee )
			End
from Rental		  join Vehicle on Vehicle.id = Rental.vid
				  join VehicleType on VehicleType.id = Vehicle.tid
				  join Customer on Customer.id = Rental.cid
where @rid = Rental.id
))

update vehicle
set bid = @bid
where vehicle.id in (select vehicle.id from rental join vehicle on @rid = rental.id)

declare @cid int
set @cid = (select cid from rental where rental.id = @rid)
declare @year int
set @year = (select year(pickupDate) from rental where rental.id = @rid)
if (select count(*) from rental where (cid = @cid) and (year(pickupDate) = @year)) >= 3 and @cid not in (select id from GoldCustomer)
begin
insert into GoldCustomer values(@cid)
end

GO