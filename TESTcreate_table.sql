use car_rental_system;

drop table GoldCustomer
drop table RentalReturn
drop table Rental
drop table Employee
drop table Vehicle
drop table Branch
drop table Customer
drop table VehicleType

create table Customer 
(
	id int identity primary key,
	--name
	fName varchar(30) not null,
	mName varchar(30), --Customer could potentially have no middle name
	lName varchar(30) not null,
	--address cannot be left as NULL value
	"address" varchar(30) not null,
	city varchar(30) not null,
	provice varchar(30) not null,
	country varchar(30) not null,
	
	--CC must be 16 digits, CC is not a required attribute for customer.
	creditCard varchar(16),
	CONSTRAINT chk_16digit check(DATALENGTH([creditCard]) = 16)
 );
create table GoldCustomer
(
	id int primary key,
	--
	--isGold bit,
	foreign key (id) references Customer(id)
);

create table Branch
(
	id int identity primary key,
	longitude float not null,
	latitude float not null,
	location varchar(30) not null,
);

create table Employee
(
	id int identity primary key,
	-- Employee MUST work at branch
	bid int not null,
	--name
	fName varchar(30)not null,
	mName varchar(30), --Employee could potentially have no middle name
	lName varchar(30)not null,

	foreign key (bid) references Branch(id)
);

create table VehicleType
(
	id int identity primary key,
	cost money,
	lateFee money,
	branchFee money,
	carType varchar(30), --candidate for primary key; could replace entirely
);

create table Vehicle
(
	id int identity primary key,
	-- Vehicle MUST have vehicle type (tid)
	tid int not null,
	-- Vehicle MUST be assigned a branch
	bid int not null,
	--
	make varchar(30) not null,
	model varchar(30) not null,
	
	-- Standard VIN length in North America is 17 digits
	-- FORMAT ||1-3  (manufacturer number)
	--        ||4-9  (Vehicle characteristics)
	--        ||10-17(unique identification of vehicle)
	vin varchar(17),


	foreign key (tid) references VehicleType(id),
	foreign key (bid) references Branch(id),	
	CONSTRAINT validVIN CHECK(DATALENGTH([vin]) = 17)
);

create table Rental
(
	id int identity primary key,
	eid int not null,
	bid int not null,
	cid int not null,
	vid int not null,
	--Procedure: corresponding tables (foreign key is not enough)
	--
	dueDate DATE not null,
	pickupDate DATE not null,

	foreign key (eid) references Employee(id),
	foreign key (bid) references Branch(id),
	foreign key (cid) references Customer(id),
	foreign key (vid) references Vehicle(id),

	constraint chkDate check(datediff(day, pickupDate, dueDate) >0)
);

create table RentalReturn
(
	rid int primary key,
	bid int not null,
	eid int not null,
	--
	dateReturned DATE not null,
	totalCost money

	-- cost must be a function of 3 values:
	-- (dueDate - pickUpDate) is Value (x) 
	-- the # of days multiplied by the daily rate from
	-- vehicleType relation
	-- (returnDate - dueDate) is Value (y) the # of days the rental is overdue,
	-- multiplied by the lateRate from vehicleType relation
	-- (different branch 
	-- (if returnRental.BID != rental.BID) value (Z) is added
	-- to the rental cost, BUT if returnRental.CID IN goldCust Z = 0

	foreign key (rid) references Rental(id),
	foreign key (bid) references Branch(id),
	foreign key (eid) references Employee(id),
)