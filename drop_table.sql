USE [car_rental_system]
GO
ALTER TABLE [dbo].[Vehicle] DROP CONSTRAINT [FK__Vehicle__tid__32AB8735]
GO
ALTER TABLE [dbo].[Vehicle] DROP CONSTRAINT [FK__Vehicle__bid__339FAB6E]
GO
ALTER TABLE [dbo].[RentalReturn] DROP CONSTRAINT [FK__RentalRetur__rid__3C34F16F]
GO
ALTER TABLE [dbo].[RentalReturn] DROP CONSTRAINT [FK__RentalRetur__eid__3E1D39E1]
GO
ALTER TABLE [dbo].[RentalReturn] DROP CONSTRAINT [FK__RentalRetur__bid__3D2915A8]
GO
ALTER TABLE [dbo].[Rental] DROP CONSTRAINT [FK__Rental__vid__395884C4]
GO
ALTER TABLE [dbo].[Rental] DROP CONSTRAINT [FK__Rental__eid__367C1819]
GO
ALTER TABLE [dbo].[Rental] DROP CONSTRAINT [FK__Rental__cid__3864608B]
GO
ALTER TABLE [dbo].[Rental] DROP CONSTRAINT [FK__Rental__bid__37703C52]
GO
ALTER TABLE [dbo].[GoldCustomer] DROP CONSTRAINT [FK__GoldCustomer__id__29221CFB]
GO
ALTER TABLE [dbo].[Employee] DROP CONSTRAINT [FK__Employee__bid__2DE6D218]
GO
/****** Object:  Table [dbo].[VehicleType]    Script Date: 04/06/2019 10:23:46 AM ******/
DROP TABLE [dbo].[VehicleType]
GO
/****** Object:  Table [dbo].[Vehicle]    Script Date: 04/06/2019 10:23:46 AM ******/
DROP TABLE [dbo].[Vehicle]
GO
/****** Object:  Table [dbo].[RentalReturn]    Script Date: 04/06/2019 10:23:46 AM ******/
DROP TABLE [dbo].[RentalReturn]
GO
/****** Object:  Table [dbo].[Rental]    Script Date: 04/06/2019 10:23:46 AM ******/
DROP TABLE [dbo].[Rental]
GO
/****** Object:  Table [dbo].[GoldCustomer]    Script Date: 04/06/2019 10:23:46 AM ******/
DROP TABLE [dbo].[GoldCustomer]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 04/06/2019 10:23:46 AM ******/
DROP TABLE [dbo].[Employee]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 04/06/2019 10:23:46 AM ******/
DROP TABLE [dbo].[Customer]
GO
/****** Object:  Table [dbo].[Branch]    Script Date: 04/06/2019 10:23:46 AM ******/
DROP TABLE [dbo].[Branch]
GO
USE [master]
GO
/****** Object:  Database [car_rental_system]    Script Date: 04/06/2019 10:23:46 AM ******/
DROP DATABASE [car_rental_system]
GO
