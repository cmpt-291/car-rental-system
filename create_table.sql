use car_rental_system

drop table GoldCustomer
drop table RentalReturn
drop table Rental
drop table Employee
drop table Vehicle
drop table Branch
drop table Customer
drop table VehicleType

create table Customer 
(
	id int identity primary key,
	--name
	fName varchar(30),
	mName varchar(30),
	lName varchar(30),
	--address
	"address" varchar(30),
	city varchar(30),
	provice varchar(30),
	country varchar(30),
	--
	creditCard int,
)

create table GoldCustomer
(
	id int primary key,
	--

	foreign key (id) references Customer(id)
)


create table Branch
(
	id int identity primary key,
	longitude float,
	latitude float,
	location varchar(30),
)

create table Employee
(
	id int identity primary key,
	bid int,
	--name
	fName varchar(30),
	mName varchar(30),
	lName varchar(30),

	foreign key (bid) references Branch(id)
)

create table VehicleType
(
	id int identity primary key,
	cost money,
	lateFee money,
	branchFee money,
	carType varchar(30), --candidate for primary key; could replace entirely
)

create table Vehicle
(
	id int identity primary key,
	tid int,
	bid int,
	--
	make varchar(30),
	model varchar(30),
	vin varchar(30), --candidate for primary key; could replace entirely

	foreign key (tid) references VehicleType(id),
	foreign key (bid) references Branch(id)
)

create table Rental
(
	id int identity primary key,
	eid int,
	bid int,
	cid int,
	vid int,
	--
	dueDate DATE,
	pickupDate DATE,

	foreign key (eid) references Employee(id),
	foreign key (bid) references Branch(id),
	foreign key (cid) references Customer(id),
	foreign key (vid) references Vehicle(id),
)

create table RentalReturn
(
	rid int primary key,
	bid int,
	eid int,
	--
	dateReturned DATE,
	totalCost money,
	
	foreign key (rid) references Rental(id),
	foreign key (bid) references Branch(id),
	foreign key (eid) references Employee(id),
)