
/********************************************************************/
/********************************************************************/
/********************************************************************/
use car_rental_system;
/********************************************************************/
/* DROPS PROCEDURES IF ALREADY EXISTING */
drop procedure getVehicleAtBranch
drop procedure GetAllAvailable
drop procedure returnCost
drop procedure RentalRevenue
drop procedure BranchRevenue
drop procedure EmployeeRevenue
go
/********************************************************************/
/********************************************************************/

/********************************************************************/
/*
GetVehicleAtBranch Procedure:
	takes an int as input. The input is queried as a BRANCH.ID
	Returns a table listing all Vehicles currently at a branch
*/

CREATE PROCEDURE GetVehicleAtBranch @bid int AS

(	select *
	from vehicle
	where bid = @bid)
except 
(	select vehicle.id, tid, vehicle.bid, make, model, vin 
	from vehicle join rental on rental.vid = vehicle.id
	where rental.id not in (select rid from rentalreturn))
GO
/********************************************************************/


/********************************************************************/
/* RETURNCOST Procedure:
		Takes 4 inputs (3 ints and one date)
		This procedure takes in the values to be inserted into the
		ReturnRental Table. Before inserting to the database it calculates
		the total cost of the rental based on 3 Factors: 
							-Time Rented (in days) * the daily rate
							-Time Overdue(in days) * the late daily rate
							-Branch Returned: Will apply the branch fee 
								if the rental is returned to a different
								branch unless the Customer is a Gold Customer

*/
create procedure ReturnCost @rid int, @bid int, @eid int, @dateReturned date
as
insert into RentalReturn values(@rid, @bid, @eid, @dateReturned, 
(
select        Case
            When Rental.bid != @bid and Customer.id not in (select GoldCustomer.id from GoldCustomer)
                Then    
                    (datediff(day, pickupDate, dueDate) * cost) + 
                    (datediff(day, dueDate, @dateReturned) * lateFee ) +
                    (branchFee)
                Else
                    (datediff(day, pickupDate, dueDate) * cost) + 
                    (datediff(day, dueDate, @dateReturned) * lateFee )
            End
from Rental       join Vehicle on Vehicle.id = Rental.vid
                  join VehicleType on VehicleType.id = Vehicle.tid
                  join Customer on Customer.id = Rental.cid
where @rid = Rental.id
))
		update vehicle
		set bid = @bid
		where vehicle.id in (select vehicle.id from rental join vehicle on @rid = rental.id)

		declare @cid int
		set @cid = (select cid from rental where rental.id = @rid)
		declare @year int
		set @year = (select year(pickupDate) from rental where rental.id = @rid)
		if	(select count(*) from rental where (cid = @cid) and 
			(year(pickupDate) = @year)) >= 3 and 
			@cid not in (select id from GoldCustomer)
		begin
		insert into GoldCustomer values(@cid)
		end
GO
/********************************************************************/

/********************************************************************/
/* Procedure: RentalRevenue:
	Takes three inputs: ( start date: date, end date: date <yyyy-mm-dd> and Vehicletype
	Returns a relation with the total revenue generated
	by that type of vehicle from rentals in the given time period */

create procedure RentalRevenue @startDate date, @endDate date, @type varchar(30)
as
select carType, count(carType) as [COUNT], sum(rentalReturn.totalCost) as [TOTAL REVENUE]
from rental, vehicle, vehicletype, rentalReturn
where	 rental.vid = vehicle.id and
		 rental.id = rentalReturn.rid and
		 vehicle.tid = vehicleType.id and
		 @startDate < dateReturned and dateReturned < @endDate 
		 and carType = @type
group by carType
go
/********************************************************************/
/* Procedure: BranchRevenue:
	Takes three inputs: ( start date: date, end date: date <yyyy-mm-dd> and Branch ID: integer)
	Returns a relation with the total revenue generated
	by that Branch in the given time period, including the Branch's
	location(for ease of reader) */


create procedure BranchRevenue @startDate date, @endDate date, @bid int
as
select branch.id, location, [NO. OF RENTALS], [TOTAL REVENUE]
from(
select	branch.id,
		(select count(*) from rental group by bid having bid = @bid) as [NO. of RENTALS], 
		sum(rentalReturn.totalCost) as [TOTAL REVENUE]
from rental, branch, rentalReturn
where	rental.bid = branch.id and
		rental.id = rentalReturn.rid and
		@startDate < dateReturned and dateReturned < @endDate
		and @bid = rental.bid
group by branch.id
)as temp, branch
where branch.id = @bid
go
/********************************************************************/


/********************************************************************/
/* Procedure: EmployeeRevenue:
	Takes three inputs: ( start date: date, end date: date <yyyy-mm-dd> and Employee ID: integer)
	Returns a relation with the total revenue generated by
	an employee from rentals in the given time period. Relation
	includes Employee's full name so they can be given the
	respect they deserve */
create procedure EmployeeRevenue @startDate date, @endDate date, @eid int 
as

select employee.id, fName + ' ' + mName + ' ' + lName as [EMPLOYEE NAME], [NO. OF RENTALS], [TOTAL REVENUE]
from(select employee.id,
	(select count(*) from rental group by eid having eid = @eid) as [NO. OF RENTALS],
		sum(rentalReturn.totalCost) as [TOTAL REVENUE]
	from rental, employee, rentalReturn
	where rental.eid = employee.id and
	  rental.id = rentalReturn.rid and
	  @startDate < dateReturned and dateReturned < @endDate and 
	  rental.eid = @eid
	group by employee.id
) as temp, employee
where employee.id = temp.id
go
/********************************************************************/
/********************************************************************/
/********************************************************************/
